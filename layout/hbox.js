"use strict";


export const AlignLeft      = "AlignLeft";  
export const AlignRight     = "AlignRight";
export const AlignCenter    = "AlignCenter";
export const AlignWidth     = "AlignWidth";
export const AlignTop       = "AlignTop";
export const AlignBottom    = "AlignBottom";
export const AlignNone      = "AlignNone";
export const AlignHeight    = "AlignHeight";


export class HBox {

    constructor(panel) {

        this._panel = panel;        // target panel
        this._spacing = 0;          // spacing between children
        this._align = AlignLeft;    // horizontal alignment of the children
        this._autoHeight = false;   // auto height state
        this._autoWidth = false;    // auto width state
    }

    // Returns the default layout parameters for children of the panel with this layout.
    childParams() {

        return Object.seal({expand: 0, align: AlignTop});
    }

    // Sets the horizontal spacing between the items in pixels and updates the layout.
    setSpacing(spacing) {
       
        this._spacing = spacing;
        this.update();
    }

    // Sets the horizontal alignment of the whole group of items
    // inside the parent panel and updates the layout.
    // This only has any effect if there are no expanded items.
    // The valid values are:
    // AlignLeft: align the group of children to the left if the panel width is
    // greater the the sum of the children widths + spacing.
    // AlignCenter: align the group of children in the center if the panel width is
    // greater the the sum of the children widths + spacing.
    // AlignRight: align the group of children to the right if the panel width is
    // greater the the sum of the children widths + spacing.
    // AlignWidth: align the individual children with the same same space between each other.
    setAlign(align) {

        this._align = align;
        this.update();
    }

    // Set if the panel minimum height should be the height of
    // the largest of its children's height.
    setAutoHeight(state) {

        this._autoHeight = state;
        this.update();
    }

    // Sets if the panel minimum width should be sum of its
    // children's width plus the spacing
    setAutoWidth(state) {

        this._autoWidth = state;
        this.update();
    }

    // Updates the layout
    update() {

        // Fast path for panel with no children
        const nchildren = this._panel.children().length;
        if (nchildren === 0) {
            return;
        }

        // If autoHeight is set, get the maximum height of all the panel's children
        // and if the panel content height is less than this maximum, set its content height to this value.
        if (this._autoHeight) {
            let maxHeight = 0;
            for (const child of this._panel.children()) {
                if (!child.visible()) {
                    continue;
                }
                if (child.height() > maxHeight) {
                    maxHeight = child.height();
                }
            }
            if (this._panel.contentHeight() < maxHeight) {
                this._panel.setContentSize(this._panel.contentWidth(), maxHeight, false);
            }
        }

        // If autoWidth is set, get the sum of widths of this panel's children plus the spacings.
        // If the panel content width is less than this width, set its content width to this value.
        if (this._autoWidth) {
            let totalWidth = 0;
            for (const child of this._panel.children()) {
                if (!child.visible()) {
                    continue;
                }
                totalWidth += child.width();
            }
            // Adds spacing
            totalWidth += this._.spacing * (nchildren-1);
            if (this._panel.contentWidth() < totalWidth) {
                this._panel.setContentSize(totalWidth, this._panel.contentHeight(), false);
            }
        }

        // Calculates the total width, expanded width, fixed width and
        // the sum of the expand factor for all items.
        let twidth = 0;
        let fwidth = 0;
        let texpand = 0;
        let ecount = 0;
        for (let pos = 0; pos < nchildren; pos++) {
            const child = this._panel.children()[pos];
            if (!child.visible()) {
                continue;
            }
            // Get child layout parameters
            const params = child.layoutParams();
            // Calculate total width
            twidth += this._panel.width();
            if (pos > 0) {
                twidth += this._spacing;
            }
            // Calculate width of expanded items
            if (params.expand > 0) {
                texpand += params.expand;
                ecount++;
                // Calculate width of fixed items
            } else {
                fwidth += this._panel.width();
                if (pos > 0) {
                    fwidth += this._spacing;
                }
            }
        }

        // If there is at least on expanded item, all free space will be occupied
        let spaceMiddle = this._spacing;
        let posX = 0;
        if (texpand > 0) {
            // If there is free space, distribute space between expanded items
            let totalSpace = this.contentWidth() - twidth;
            if (totalSpace > 0) {
                for (const child of this._panel.children()) {
                    if (!child.visible()) {
                        continue;
                    }
                    // Get child layout parameters
                    const params = child.layoutParams();
                    if (params.expand > 0) {
                        let iwidth = totalSpace * params.expand / texpand;
                        child.setWidth(child.width() + iwidth);
                    }
                }
                // No free space: distribute expanded items widths
            } else {
                for (const child of this._panel.children()) {
                    if (!child.visible()) {
                        continue;
                    }
                    // Get child layout parameters
                    const params = child.layoutParams();
                    if (params.expand > 0) {
                        const spacing = this._spacing * (ecount - 1);
                        const iwidth = (this._panel.contentWidth() - spacing - fwidth - this._spacing) * params.expand / texpand;
                        child.setWidth(iwidth);
                    }
                }
            }
            // No expanded items: checks block horizontal alignment
        } else {
            // Calculates initial x position which depends
            // on the current horizontal alignment.
            switch (this._align) {
            case AlignLeft:
                posX = 0;
                break;
            case AlignCenter:
                posX = (this._panel.contentWidth() - twidth) / 2;
                break;
            case AlignRight:
                posX = this._panel.contentWidth() - twidth;
                break;
            case AlignWidth: {
                let space = this._panel.contentWidth() - twidth + this._spacing*(nchildren-1);
                if (space < 0) {
                    this._space = this._spacing * (nchildren-1);
                }
                spaceMiddle = space / (nchildren + 1);
                posX = spaceMiddle;
            }
                break;
            default:
                throw new Error("HBox layout with invalid global horizontal alignment");
            }
        }

        // Calculates the Y position of each item considering its vertical alignment
        let posY = 0;
        const height = this._panel.contentHeight();
        for (let pos = 0; pos < nchildren; pos++) {
            const child = this._panel.children()[pos];
            if (!child.visible()) {
                continue;
            }
            const params = child.layoutParams();
            const cheight = this._panel.height();
            switch (params.align) {
            case AlignNone:
            case AlignTop:
                posY = 0;
                break;
            case AlignCenter:
                posY = (height - cheight) / 2;
                break;
            case AlignBottom:
                posY = height - cheight;
                break;
            case AlignHeight:
                posY = 0;
                this._panel.setHeight(height);
                break;
            default:
                throw new Error("HBox layout with invalid child vertical alignment");
            }
            // Sets the child position
            child.setPos(posX, posY);
            // Calculates next child position
            posX += child.width();
            if (pos < nchildren - 1) {
                posX += spaceMiddle;
            }
        }
    }
}

