"use strict";

import * as font   from "../core/font";
import * as color  from "../core/color";
import * as rect   from "../core/rect";
import * as util   from "../util/clone";
import * as icon   from "../assets/icons";
import * as consts from "../widgets/consts";

class Light {

    constructor() {

        // Global defaults
        this.textFont           = font.New(14, font.UnitPx, font.FamilySansSerif),
        this.iconFont           = font.New(14, font.UnitPx, "Material Icons"),
        this.fgColor            = color.New("black"),
        this.bgColor            = color.New("lightgray"),
        this.borderColor        = color.New("gray");
        this.borderRound        = [4, 1];
        this.fgColorDisabled    = color.New("gray");

        // Label
        this.label = {
            padding:    new rect.Border(2, 1, 0, 1),
            bgColor:    color.New(0,0,0,0),     // transparent
            fgColor:    this.fgColor,
            font:       this.textFont,
        };

        // Button
        this.button = {
            textFont:   this.textFont,
            iconFont:   this.iconFont,
            iconPos:    consts.IconLeft,
        };
        this.button.normal = {
            border:         new rect.Border(1, 1, 1, 1),
            padding:        new rect.Border(2, 1, 1, 1),
            borderRound:    this.borderRound,
            borderColor:    this.borderColor,
            bgColor:        this.bgColor,
            fgColor:        this.fgColor,
        };
        this.button.over = util.clone(this.button.normal);
        this.button.over.bgColor.mult(1.05);
        this.button.over.fgColor = color.New("blue");
        this.button.focus = util.clone(this.button.over);
        this.button.pressed = util.clone(this.button.normal);
        this.button.pressed.bgColor.mult(1.1);
        this.button.disabled = util.clone(this.button.normal);

        // Checkbox
        this.checkbox = {
            textFont:       this.textFont,
            iconFont:       this.iconFont,
            iconPos:        consts.IconLeft,
            iconCheck:      icon.CheckBox,
            iconNoCheck:    icon.CheckBoxOutlineBlank,
        };
        this.checkbox.normal = {
            border:         new rect.Border(0, 0, 0, 0),
            padding:        new rect.Border(2, 1, 1, 1),
            borderColor:    this.borderColor,
            //bgColor:        this.bgColor,
            bgColor:        color.New(0,0,0,0),
            fgColor:        this.fgColor,
        };
        this.checkbox.over = util.clone(this.checkbox.normal);
        this.checkbox.over.bgColor.mult(1.05);
        this.checkbox.over.fgColor = color.New("blue");
        this.checkbox.focus = util.clone(this.checkbox.over);
        this.checkbox.pressed = util.clone(this.checkbox.normal);
        this.checkbox.pressed.bgColor.mult(1.1);
        this.checkbox.disabled = util.clone(this.checkbox.normal);

        // Checkbox
        this.radio = {
            textFont:       this.textFont,
            iconFont:       this.iconFont,
            iconPos:        consts.IconLeft,
            iconCheck:      icon.RadioButtonChecked,
            iconNoCheck:    icon.RadioButtonUnchecked,
        };
        this.radio.normal = {
            border:         new rect.Border(0, 0, 0, 0),
            padding:        new rect.Border(2, 1, 1, 1),
            borderColor:    this.borderColor,
            //bgColor:        this.bgColor,
            bgColor:        color.New(0,0,0,0),
            fgColor:        this.fgColor,
        };
        this.radio.over = util.clone(this.radio.normal);
        this.radio.over.bgColor.mult(1.05);
        this.radio.over.fgColor = color.New("blue");
        this.radio.focus = util.clone(this.radio.over);
        this.radio.pressed = util.clone(this.radio.normal);
        this.radio.pressed.bgColor.mult(1.1);
        this.radio.disabled = util.clone(this.radio.normal);

        // Tuner
        this.tuner = {
            textFont:       font.New(48, font.UnitPx, font.FamilySansSerif),
        };
        this.tuner.normal = {
            border:         new rect.Border(1, 1, 1, 1),
            padding:        new rect.Border(1, 1, 0, 1),
            borderRound:    this.borderRound,
            borderColor:    this.borderColor,
            bgColor:        this.bgColor,
            fgColor:        this.fgColor,
            digit: {
                leftZeros: {
                    bgColor: this.bgColor,
                    fgColor: color.New("gray"),
                },
                bgColor:    this.bgColor,
                fgColor:    this.fgColor,
            }
        };
        this.tuner.disabled = util.clone(this.tuner.normal);
        this.tuner.disabled.fgColor = this.fgColorDisabled;
        this.tuner.over = util.clone(this.tuner.normal);
        this.tuner.over.digit.bgColor = color.New("white");
        //this.tuner.over.digit.fgColor = color.New("blue");
        this.tuner.focus = util.clone(this.tuner.over);
    }
}

export const light = Object.freeze(new Light());
console.log(light);

