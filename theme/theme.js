"use strict";

import * as util from "../util/clone";
import {light} from "./light";

var current = light;

export function theme() {

    return current;
}

// Returns a copy of the style object for the specified klasswidget of the current theme.
export function get(klass) {

    const style = current[klass];
    if (!style) {
        throw new Error("Style not found for:" + klass);
    }
    return util.clone(current[klass]);
}

