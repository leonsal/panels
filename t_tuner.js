"use strict";

import * as panel from "./core/panel";
import * as color from "./core/color";
import * as renderer from "./core/renderer";
import * as evmanager from "./core/event_manager";
import * as tuner from "./widgets/tuner";

export function run() {

    let body = document.getElementsByTagName("body");

    let canvas1 = document.createElement("canvas");
    canvas1.width = 1200;
    canvas1.height = 400;
    body[0].appendChild(canvas1);
    let ctx1 = canvas1.getContext("2d");
    ctx1.fillStyle = "lightgray";
    ctx1.fillRect(0, 0, canvas1.width, canvas1.height);

    const root1 = panel.New(0, 0);
    root1.setBorder(1,1,1,1);
    root1.setBgColor(color.New("white"));
    root1.setBorderColor(color.New("black"));
    root1.setSize(canvas1.width, canvas1.height);

    const t1 = tuner.New(7, " Hz");
    t1.setValue(1234567);
    t1.setPos(10, 10);
    //t1.setValueRange(100, Math.pow(10, 7)-100);
    t1.subscribe(evmanager.OnChange, (v) => console.log("tuner1:", v));
    root1.add(t1);

    const t2 = tuner.New(6, " Hz");
    t2.setValue(654321);
    t2.setPos(10, 100);
    t2.styles.textFont.size = 32;
    t2.subscribe(evmanager.OnChange, (v) => console.log("tuner2:", v));
    root1.add(t2);

    // Starts rendering
    const rend = renderer.New(canvas1, root1);
    rend.start();

    // Starts event manager
    const evm = evmanager.Get();
    evm.add(canvas1, root1);
    evm.start();
}



