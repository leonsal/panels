"use strict";


export const StyleNormal        = "normal";
export const StyleItalic        = "italic";
export const StyleOblique       = "oblique";
export const WeightNormal       = "normal";
export const WeightBold         = "bold";
export const WeightBolder       = "bolder";
export const WeightLighter      = "lighter";
export const Weight100          = "100";
export const Weight200          = "200";
export const Weight300          = "300";
export const Weight400          = "400";
export const Weight500          = "500";
export const Weight600          = "600";
export const Weight700          = "700";
export const Weight800          = "800";
export const Weight900          = "900";
export const UnitPx             = "px";
export const UnitPt             = "pt";
export const FamilySerif        = "serif";
export const FamilySansSerif    = "sans-serif";
export const FamilyCursive      = "cursive";
export const FamilyFantasy      = "fantasy";
export const FamilyMonospace    = "monospace";

export function New(size, unit=UnitPx, family=FamilySerif) {

    return Object.seal(new Font(size, unit, family));
}

export function clone(font) {

    const f = New(font.size);
    return Object.assign(f, font);
}

export class Font {

    constructor(size, unit=UnitPx, family=FamilySerif) {

        this._style     = StyleNormal;      // normal, italic, oblique
        this._weight    = WeightNormal;     // normal, bold, bolder, lighter, 100,...,900
        this._size      = size;             // size in pixels or points
        this._sizeUnit  = unit;             // px or pt
        this._family    = family;           // serif, sans-serif, cursive, fantasy, monospace, others
        this._str       = "";               // cached formatted font string
        this._height    = 0;                // cached calculated height
    }

    get style() {

        return this._style;
    }

    set style(v) {

        this._style = v;
        this._reset();
    }

    get weight() {

        return this._weight;
    }

    set weight(v) {

        this._weight = v;
        this._reset();
    }

    get size() {

        return this._size;
    }

    set size(v) {

        this._size = v;
        this._reset();
    }

    get sizeUnit() {

        return this._sizeUnit;
    }

    set sizeUnit(v) {

        this._sizeUnit = v;
        this._reset();
    }

    get family() {

        return this._family;
    }

    set family(v) {

        this._family = v;
        this._reset();
    }

    setFrom(other) {

        Object.assign(this, other);
    }

    clone() {
        
        const c = New(this._size);
        return Object.assign(c, this);
    }

    toString() { 

        if (this._str == "") {
            this._str = [
                this._style,
                this._weight,
                this._size.toString() + this._sizeUnit,
                this._family,
            ].join(" ");
        }
        return this._str;
    }

    height() {

        if (this._height != 0) {
            return this._height;
        }

        const body = document.getElementsByTagName("body")[0];
        const div = document.createElement("div");
        div.innerHTML = "M";
        const style = div.style;
        style.position      = "absolute";
        style.top           = "-100px";
        style.left          = "-100px";
        style.fontFamily    = this._family;
        style.fontWeight    = this._weight;
        style.fontSize      = this.size.toString() + this._sizeUnit;

        body.appendChild(div);
        const result = div.offsetHeight;
        body.removeChild(div);

        this._height = result;
        return this._height;
    }

    _reset() {
        this._str = "";
        this._height = 0;
    }
}

