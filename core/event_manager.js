"use strict";


// Event names
export const OnClick           = "OnClick";         // generated event
export const OnChange          = "OnChange";        // generated event
export const OnLostKeyFocus    = "OnLostKeyFocus";  // generated event
export const OnKeyDown         = "keydown";         // Javascript native event
export const OnMouseDown       = "mousedown";       // Javascript native event
export const OnMouseUp         = "mouseup";         // Javascript native event
export const OnMouseEnter      = "OnMouseEnter";    // generated event
export const OnMouseLeave      = "OnMouseLeave";    // generated event
export const OnMouseOut        = "OnMouseOut";      // generated event
export const OnWheel           = "wheel";           // Javacript native event

// Instance of EventManager singleton
var instance;

// Returns the instance of the EventManager singleton
export function Get() {

    if (instance !== undefined) {
        return instance;
    }
    instance = Object.seal(new EventManager());
    return instance;
}

export class EventManager {

    constructor() {

        this._canvases = new Map();                 // maps canvas to state containing associated panel and callbacks.
        this._started = false;                      // manager started state
        this._mouseFocus = null;                    // current panel with the mouse focus (in any canvas)
        this._keyFocus = null;                      // current panel with the key focus (in any canvas)
        this._modalPanel = null;                    // current modal panel
        this._cbKeydown = (ev) => this._onKey(ev);  // keydown callback
        this._cbKeyup = (ev) => this._onKey(ev);    // keyup callback
        this._cbKeypress = (ev) => this._onKey(ev); // keypress callback
    }

    // Add a canvas and panel to be managed by the event manager.
    add(canvas, panel) {

        let state = this._canvases.get(canvas);
        if (state === undefined) {
            state = Object.freeze({cb:{}, panel:panel});
            this._canvases.set(canvas, state);
        } else {
            state.panel = panel;
        }
    }

    // Remove the specified canvas from the event manager.
    remove(canvas) {

        let state = this._canvases.get(canvas);
        if (state === undefined) {
            return;
        }
        this._removeListeners(canvas);
    }


    // Starts the event manager which add event listeners and dispatch
    // received events to the managed panels.
    start() {

        if (this._started) {
            return;
        }
        window.addEventListener("keydown", this._cbKeydown);
        window.addEventListener("keyup", this._cbKeyup);
        window.addEventListener("keypress", this._cbKeypress);
        for (let canvas of this._canvases.keys()) {
            this._addListeners(canvas);
        }
        this._started = true;
    }

    // Stops the event manager which removes previously installed event listeners
    // and stops dispatching events to the managed panels.
    stop() {

        if (!this._started) {
            return;
        }
        window.removeEventListener("keydown", this._cbKeydown);
        window.removeEventListener("keyup", this._cbKeyup);
        window.removeEventListener("keypress", this._cbKeypress);
        for (let canvas of this._canvases.keys()) {
            this._removeListeners(canvas);
        }
        this._started = false;
    }

    // Clears the key focus panel (if any) without
    // sending OnLostKeyFocus for previous focused panel
    clearKeyFocus() {

        this._keyFocus = null;
    }

    // Sets the panel which will receive all keyboard events
    // Passing null will remove the focus (if any)
    setKeyFocus(panel) {
        
        if (this._keyFocus !== null) {
            // If this panel is already in focus, nothing to do
            if (panel !== null && this._keyFocus === panel) {
                return;
            }
            // Sends event to panel which has lost focus
            this._keyFocus._onEvent(this, {type:OnLostKeyFocus});
        }
        this._keyFocus = panel;
    }

    // Sets the panel which will receive all mouse events
    // Passing null will restore the default event processing
    setMouseFocus(panel) {

        this._mouseFocus = panel;
    }

    // Sets the modal panel.
    // If there is a modal panel, only events for this panel are dispatched
    // To remove the modal panel call this function with a nulll panel.
    setModal(panel) {
    
        this._modalPanel = panel;
    }

    //// Sets the panel which will receive all scroll events
    //// Passing nil will restore the default event processing
    //setWheelFocus(panel) {
    //
    //  r.scrollFocus = ipan
    //}

    _addListeners(canvas) {

        const state = this._canvases.get(canvas);
        state.cb["mousedown"] = (ev) => {
            this._onMouse(ev, canvas, state.panel);
        };
        canvas.addEventListener("mousedown", state.cb["mousedown"]);
        state.cb["mouseup"] = (ev) => {
            this._onMouse(ev, canvas, state.panel);
        };
        canvas.addEventListener("mouseup", state.cb["mouseup"]);
        state.cb["mousemove"] = (ev) => {
            this._onMouse(ev, canvas, state.panel);
        };
        canvas.addEventListener("mousemove", state.cb["mousemove"]);
        state.cb["wheel"] = (ev) => {
            this._onMouse(ev, canvas, state.panel);
        };
        canvas.addEventListener("wheel", state.cb["wheel"]);
    }

    _removeListeners(canvas) {

        const state = this._canvases.get(canvas);
        canvas.removeEventListener("mousedown", state.cb["mousedown"]);
        canvas.removeEventListener("mouseup", state.cb["mouseup"]);
        canvas.removeEventListener("mousemove", state.cb["mousemove"]);
        canvas.removeEventListener("wheel", state.cb["wheel"]);
    }

    // Returns mouse position relative to the canvas for the specified event
    _mousePos(ev, canvas) { 

        const rect = canvas.getBoundingClientRect();
        return {
            x: ev.clientX - rect.left,
            y: ev.clientY - rect.top
        };
    }

    // Receives "mousedown", "mouseup", "mousemove" and "wheel" events
    // and dispatch them to the panels under the mouse position.
    _onMouse(ev, canvas, panel) {

        const rect = canvas.getBoundingClientRect();
        ev.canvasX = ev.clientX - rect.left;
        ev.canvasY = ev.clientY - rect.top;
        this._sendPanels(ev, panel);
    }

    // Called when any key event is received
    _onKey(ev) {

        // If no panel has the key focus, nothing to do
        if (this._keyFocus === null) {
            return;
        }
        // Checks modal panel
        if (!this._canDispatch(this._keyFocus)) {
            return;
        }

        // Dispatch key event to focused panel
        this._keyFocus._onEvent(this, ev);
    }

    // Returns if event can be dispatched to the specified panel.
    // An event cannot be dispatched if there is a modal panel and the specified
    // panel is not the modal panel or any of its children.
    _canDispatch(panel) {
    
        if (this._modalPanel === null) {
            return true;
        }
        if (this._modalPanel === panel) {
            return true;
        }
        if (this._modalPanel.isDescendant(panel)) {
            return true;
        }
        return false;
    }

    // Sends a mouse event to the currently focused panel or panels
    // which contain the specified canvas position
    _sendPanels(ev, root) {

        // If there is panel with mouse focus send only to this panel
        if (this._mouseFocus !== null) {
            // Checks modal panel
            if (!this._canDispatch(this._mouseFocus)) {
                return;
            }
            this._mouseFocus._onEvent(this, ev);
            return;
        }

        // list of panels which contains the mouse position
        const targets = [];

        // Checks recursively if the specified panel and
        // any of its children contain the mouse position
        const checkPanel = (p) => {

            // If panel not visible or not enabled, ignore
            if (!p.visible() || !p.enabled()) {
                return;
            }
            // Checks if this panel contains the mouse position
            const found = p.insideBorders(ev.canvasX, ev.canvasY);
            if (found) {
                targets.push(p);
            }
            else {
                // If OnMouseEnter previously sent, sends OnMouseLeave with a nil event
                if (p._mouseEnter) {
                    p._onEvent(this, {type: OnMouseLeave});
                    p._mouseEnter = false;
                }
                // If mouse button was pressed, sends event informing mouse down outside of the panel
                if (ev.type === "mousedown") {
                    p._onEvent(this, {type: OnMouseOut});
                }
            }
            // Checks if any of its children also contains the position
            p.children().forEach(checkPanel);
        };

        // Checks the root panel and all of its children building targets list.
        checkPanel(root);

        // No panels found
        if (targets.length === 0) {
            // If event is mouse click, removes the keyboard focus
            if (ev.type === "mousedown") {
                this.setKeyFocus(null);
            }
            return;
        }

        // Sorts panels by absolute z with the most foreground (greater Z) panels first
        const compareFn = (a, b) => {
            if (a._z < b._z) {
                return -1;
            }
            if (a._z > b._z) {
                return 1;
            }
            return 0;
        };
        targets.sort(compareFn);

        // Send events to panels
        let stop = false;
        for (const p of targets) {
            // Checks for modal panel
            if (!this._canDispatch(p)) {
                continue;
            }
            // Mouse move event
            if (ev.type === "mousemove") {
                stop = p._onEvent(this, ev);
                if (!p._mouseEnter) {
                    stop = p._onEvent(this, {type: OnMouseEnter});
                    p._mouseEnter = true;
                }
                // Mouse button/wheel event
            } else {
                stop = p._onEvent(this, ev);
            }
            if (stop) {
                break;
            }
        }
    }
}

