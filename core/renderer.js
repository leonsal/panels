"use strict";

import * as dispatcher from "./dispatcher";
import * as logger from "../util/logger";

export const OnRender = "OnRender";

export function New(canvas, panel) {

    return Object.seal(new Renderer(canvas, panel));
}

export class Renderer extends dispatcher.Dispatcher {

    constructor(canvas, panel) {

        super();
        this._canvas = canvas;
        this._panel = panel;
        this._ctx = canvas.getContext("2d");
        this._started = false;
        this._log = logger.getLogger("Renderer");
    }

    // Starts the rendering of the panel in the canvas
    // if not already started.
    start() {

        if (this._started) {
            return;
        }
        this._started = true;

        // Callback for requestAnimationFrame
        const cb = (ts) => {
            if (!this._started) {
                return;
            }
            // Dispatch event before render
            this.dispatch(OnRender, ts);
            // If panel or any descendent changed, render it
            if (this._panel.treeChanged()) {
                const start = performance.now();
                this._render(this._panel, ts);
                //this._log.debug("render: %5.2fms", performance.now() - start);
            }
            window.requestAnimationFrame(cb);
        };
        window.requestAnimationFrame(cb);
    }

    stop() {

        this._started = false;
    }

    // Render the specified panel and all its children
    _render(panel, ts) {

        panel.render(this._ctx);
        for (const child of panel.children()) {
            this._render(child, ts);
        }
    }
}


