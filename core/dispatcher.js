"use strict";


export function New() {

    return Object.seal(new Dispatcher());
}

export class Dispatcher {

    constructor() {
        this._events = new Map();
    }

    // Subscribe for event
    subscribe(evname, cb, ctx = null) {

        let subs = this._events.get(evname);
        if (subs === undefined) {
            subs = [];
            this._events.set(evname, subs);
        }
        subs.push({cb, ctx});
    }

    // Unsubscribe for the specified event and callback.
    // Returns the number of subscriptions found.
    unsubscribe(evname, cb) {

        let found = 0;
        const subs = this._events.get(evname);
        if (subs === undefined) {
            return found;
        }
        let i = 0;
        while (i < subs.length) {
            if (subs[i].cb !== cb) {
                i++;
                continue;
            }
            subs.splice(i, 1);
            found++;
        }
        return found;
    }

    // Dispatch event calling all subscribed listeners
    dispatch(evname, ...args) {

        const subs = this._events.get(evname);
        if (subs === undefined) {
            return;
        }
        for (let i = 0; i < subs.length; i++) {
            const sub = subs[i];
            if (sub.ctx !== null) {
                sub.call(sub.ctx, sub.cb(...args));
            } else {
                sub.cb(...args);
            }
        }
    }

    clear() {

    }
}


