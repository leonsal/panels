"use strict";

import * as event from "../core/event_manager";
import * as theme from "../theme/theme";
import {ButtonBase} from "./button";


export function New(text="") {

    return Object.seal(new Radio(text));
}


export class Radio extends ButtonBase {

    constructor(text="", group="") {

        super(text);
        this._styles = theme.get("radio");
        this.icon = this._styles.iconNoCheck;
        this._value = false;
        this._group = group;
    }

    get value() {

        return this._value;
    }

    set value(state) {

        if (this._value === state) {
            return;
        }
        this._value = state;
        if (this._value) {
            this.icon = this._styles.iconCheck;
        } else {
            this.icon = this._styles.iconNoCheck;
        }
        this.setChanged(true);
    }

    get group() {

        return this._group;
    }

    set group(group) {

        if (!group) {
            this._group = null;
            return;
        }
        this._group = group;
    }

    // Process events for this checkbox.
    // Called by the event manager.
    _onEvent(evm, ev) {
      
        if (ev.type === event.OnMouseUp) {
            if (this._pressed) {
                this.value = !this.value;
                this.dispatch(event.OnClick);
            }
            this._pressed = false;
            this.setChanged(true);
            return true;
        } else {
            return super._onEvent(evm, ev);
        }
    }
}
