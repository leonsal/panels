"use strict";

import * as panel from "../core/panel";
import * as label from "../widgets/label";
import * as event from "../core/event_manager";
import * as theme from "../theme/theme";
import * as consts from "./consts";

export function New(text, icon="") {

    return Object.seal(new Button(text, icon));
}

export class ButtonBase extends panel.Panel {

    constructor(text=null, icon=null) {

        super();
        this._styles = null;        // button styles got from theme
        this._text = null;          // text label
        this._icon = null;          // icon label
        if (text !== null) {        // if text supplied, creates text label
            this.text = text;
        }
        if (icon !== null) {        // if icon supplied, creates icon label
            this.icon = icon;
        }
        this._mouseOver = false;
        this._pressed = false;
    }

    // Returns the styles of this button.
    get styles() {

        return this._styles;
    }

    // Returns the current text of this button
    get text() {

        if (this._text) {
            return this._text.text;
        }
        return "";
    }

    // Sets this button text creating the text label if necessary
    set text(text) {

        if (this._text !== null) {
            this._text.text = text;
        } else {
            this._text = label.New(text);
            this.add(this._text);
        }
        this.setChanged(true);
    }

    // Sets this button icon creating the icon label if necessary
    set icon(text) {

        if (this._icon !== null) {
            this._icon.text = text;
        } else {
            this._icon = label.New(text);
            this.add(this._icon);
        }
        this.setChanged(true);
    }

    // Applies the specified style to the base panel and to
    // this button text and/or icon labels.
    _applyStyle(style) {

        // Apply style to button base panel
        super.applyStyle(style);

        // Apply style to child labels
        if (this._text) {
            this._text.style.fgColor.setFrom(style.fgColor);
            this._text.style.font.setFrom(this._styles.textFont);
        }
        if (this._icon) {
            this._icon.style.fgColor.setFrom(style.fgColor);
            this._icon.style.font.setFrom(this._styles.iconFont);
        }
    }

    // Updates the button visual style accordingly to its state.
    _update() {
    
        if (!this.enabled()) {
            this._applyStyle(this._styles.disabled);
            return;
        }
        if (this._focus) {
            this._applyStyle(this._styles.focus);
            return;
        }
        if (this._pressed) {
            this._applyStyle(this._styles.pressed);
            return;
        }
        if (this._mouseOver) {
            this._applyStyle(this._styles.over);
            return;
        }
        this._applyStyle(this._styles.normal);
    }

    render(ctx) {

        switch (this._styles.iconPos) {
        case consts.IconLeft:
            this._renderLeft(ctx);
            break;
        case consts.IconTop:
            this._renderTop(ctx);
            break;
        case consts.IconRight:
            this._renderRight(ctx);
            break;
        case consts.IconBottom:
            this._renderBottom(ctx);
            break;
        default:
            throw new Error("Invalid icon position");
        }
    }

    _renderLeft(ctx) {

        this._update();

        // Calculates the button panel minimum content area width and height 
        // to fit exactly the icon/text.
        let mwidth = 0;
        let mheight = 0;
        if (this._icon) {
            this._icon._recalc(ctx);
            mwidth += this._icon.width();
            mheight = this._icon.height();
        }
        if (this._text) {
            this._text._recalc(ctx);
            mwidth += this._text.width();
            if (this._text.height() > mheight) {
                mheight = this._text.height();
            }
        }

        // Sets the button content area
        const cwidth  = Math.max(this.contentWidth(), mwidth);
        const cheight = Math.max(this.contentHeight(), mheight);
        this.setContentSize(cwidth, cheight);

        // Centralize the position of the icon/text
        let posX = (cwidth - mwidth) / 2;
        let posY = (cheight -mheight) / 2;

        // Sets the position of the child labels: icon and text
        if (this._icon) {
            this._icon.setPos(posX, posY);
            posX += Math.round(this._icon.width());
        }
        if (this._text) {
            this._text.setPos(posX, posY);
        }
        super.render(ctx);
    }

    _renderRight(ctx) {

        this._update();

        // Calculates the button panel minimum content area width and height 
        // to fit exactly the icon/text.
        let mwidth = 0;
        let mheight = 0;
        if (this._icon) {
            this._icon._recalc(ctx);
            mwidth += this._icon.width();
            mheight = this._icon.height();
        }
        if (this._text) {
            this._text._recalc(ctx);
            mwidth += this._text.width();
            if (this._text.height() > mheight) {
                mheight = this._text.height();
            }
        }

        // Sets the button content area
        const cwidth  = Math.max(this.contentWidth(), mwidth);
        const cheight = Math.max(this.contentHeight(), mheight);
        this.setContentSize(cwidth, cheight);

        // Centralize the position of the icon/text
        let posX = (cwidth - mwidth) / 2;
        let posY = (cheight -mheight) / 2;

        // Sets the position of the child labels: icon and text
        if (this._text) {
            this._text.setPos(posX, posY);
            posX += Math.round(this._text.width());
        }
        if (this._icon) {
            this._icon.setPos(posX, posY);
        }
        super.render(ctx);
    }

    // render button with icon on top
    _renderTop(ctx) {

        this._update();

        // Calculates the button panel minimum content area width and height 
        // to fit exactly the icon/text.
        let mwidth = 0;
        let mheight = 0;
        if (this._icon) {
            this._icon._recalc(ctx);
            mwidth = this._icon.width();
            mheight += this._icon.height();
        }
        if (this._text) {
            this._text._recalc(ctx);
            if (this._text.width() > mwidth) {
                mwidth = this._text.width();
            }
            mheight += this._text.height();
        }

        // Sets the button content area
        const cwidth  = Math.max(this.contentWidth(), mwidth);
        const cheight = Math.max(this.contentHeight(), mheight);
        this.setContentSize(cwidth, cheight);

        // Centralize the position of the icon/text
        let posY = (cheight -mheight) / 2;

        // Sets the position of the child labels: icon and text
        if (this._icon) {
            let posX = (cwidth - this._icon.width()) / 2;
            this._icon.setPos(posX, posY);
            posY += Math.round(this._icon.width());
        }
        if (this._text) {
            let posX = (cwidth - this._text.width()) / 2;
            this._text.setPos(posX, posY);
        }
        super.render(ctx);
    }

    // render button with icon on bottom
    _renderBottom(ctx) {

        this._update();

        // Calculates the button panel minimum content area width and height 
        // to fit exactly the icon/text.
        let mwidth = 0;
        let mheight = 0;
        if (this._text) {
            this._text._recalc(ctx);
            mwidth = this._text.width();
            mheight += this._text.height();
        }
        if (this._icon) {
            this._icon._recalc(ctx);
            if (this._icon.width() > mwidth) {
                mwidth = this._icon.width();
            }
            mheight += this._icon.height();
        }

        // Sets the button content area
        const cwidth  = Math.max(this.contentWidth(), mwidth);
        const cheight = Math.max(this.contentHeight(), mheight);
        this.setContentSize(cwidth, cheight);

        // Centralize the position of the icon/text
        let posY = (cheight -mheight) / 2;

        // Sets the position of the child labels: icon and text
        if (this._text) {
            const posX = (cwidth - this._text.width()) / 2;
            this._text.setPos(posX, posY);
            posY += this._text.height();
        }
        if (this._icon) {
            const posX = (cwidth - this._icon.width()) / 2;
            this._icon.setPos(posX, posY);
        }
        super.render(ctx);
    }

    // Process evms for this button or any of its children.
    // Called by the event manager.
    _onEvent(evm, ev) {

        switch (ev.type) {
        case event.OnMouseEnter:
            this._mouseOver = true;
            this.setChanged(true);
            return true;
        case event.OnMouseLeave:
            this._mouseOver = false;
            this._pressed = false;
            this.setChanged(true);
            return true;
        case event.OnMouseDown:
            this._pressed = true;
            this.setChanged(true);
            return true;
        case event.OnMouseUp:
            if (this._pressed) {
                this.dispatch(event.OnClick);
            }
            this._pressed = false;
            this.setChanged(true);
            return true;
        }
        return false;
    }
}


export class Button extends ButtonBase {

    constructor(text="", icon="") {

        super(text, icon);
        this._styles = theme.get("button");
    }
}


