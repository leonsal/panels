"use strict";

import * as panel from "../core/panel";
import * as theme from "../theme/theme";
import * as util from "../util/clone";

export function New(text) {

    return Object.seal(new Label(text));
}


export class Label extends panel.Panel {

    constructor(text) {

        super();
        this._text = text;
        this._style = theme.get("label");
    }

    // Returns a reference to this label style
    get style() {

        return this._style;
    }

    // Returns the current text of this label
    get text() {

        return this._text;
    }

    // Sets the text of this label
    set text(v) {

        this._text = v;
        this.setChanged(true);
    }

    setStyle(style) {

        this._style = util.clone(style);
    }

    render(ctx) {

        // Calculates the text width and height, sets this label panel content size
        // and render the base Panel.
        this._recalc(ctx);
        super.render(ctx);

        // Draws the label text over the panel content area
        ctx.save();
        super.clipContent(ctx);
        ctx.textBaseline = "top";
        ctx.fillStyle = this._style.fgColor.toString();
        const pos = this.absContentPos();
        ctx.fillText(this._text, pos.x, pos.y);
        ctx.restore();
    }

    // Process evms for this button or any of its children.
    // Called by the event manager.
    _onEvent(evm, ev) {

        // Dispatch event to this label subscribers.
        this.dispatch(ev.type, ev);

        // Do not stop event propagation for other panels.
        return false;
    }

    // May be called by other widgets which use labels internally.
    _recalc(ctx) {

        // Apply the current label style to its base panel
        super.applyStyle(this._style);

        // Calculates label text width and height and sets its panel content size.
        ctx.font = this._style.font.toString();
        const tm = ctx.measureText(this._text);
        this.setContentSize(tm.width, this._style.font.height());
    }
}

