"use strict";

import * as color from "./core/color";
import * as panel from "./core/panel";
import * as renderer from "./core/renderer";
import * as evmanager from "./core/event_manager";
import * as button from "./widgets/button";
import * as layout from "./layout/hbox";

export function run() {

    let canvas = document.createElement("canvas");
    canvas.width = 1200;
    canvas.height = 800;
    let body = document.getElementsByTagName("body");
    body[0].appendChild(canvas);

    let ctx = canvas.getContext("2d");
    ctx.fillStyle = "lightgray";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    const root = panel.New(0, 0);
    root.setBgColor(color.New("blue"));
    root.setPos(10, 10);
    root.setBorderColor(color.New("black"));
    root.setBorder(1, 1, 1, 1);
    root.setBgColor(color.New("white"));
    root.setPadding(10, 10, 10, 10);
    root.setSize(canvas.width - 20, 350);

    const pl = panel.New(800, 200);
    pl.setPos(0, 40);
    pl.setBorderColor(color.New("black"));
    pl.setBorder(1, 1, 1, 1);
    pl.setLayout(layout.HBox);
    const hbox = pl.layout();
    console.log("hbox", hbox);
    root.add(pl);

    const b1 = button.New("Add child");
    b1.setPos(0, 0);
    b1.subscribe(evmanager.OnClick, (/*ev*/) => {
        const text = "Child" + pl.children().length.toString();
        const b = button.New(text);
        let height = b.height();
        b.setHeight(Math.floor(height + 100 * Math.random()));
        pl.add(b);
    });
    root.add(b1);


    // Starts rendering
    const rend = renderer.New(canvas, root);
    rend.start();

    // Starts event manager
    const evm = evmanager.Get();
    evm.add(canvas, root);
    evm.start();
}


