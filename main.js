"use strict";

import * as logger from "./util/logger";
import * as testCanvas from "./t_canvas";
import * as testPanel  from "./t_panel";
import * as testLabel  from "./t_label";
import * as testButton from "./t_button";
import * as testTuner  from "./t_tuner";
import * as testHBoxLayout  from "./t_hbox_layout";

var testMap = {
    "canvas":       testCanvas,
    "panel":        testPanel,
    "label":        testLabel,
    "button":       testButton,
    "tuner":        testTuner,
    "hbox_layout":  testHBoxLayout,
};



function main() {

    // Creates logger
    const log = logger.getLogger("Main");
    const hconsole = logger.createConsoleHandler({
        level:      logger.DEBUG,
        useColors:  true
    });
    log.addHandler(hconsole);
    log.setLevel("DEBUG");
    log.info("Starting...");

    // Get optional test name from url
    let search = window.location.search;
    if (search.length === 0) {
        buildMenu();
        return;
    }

    // Get test module from test name
    let name = search.substring(3);
    let test = testMap[name];
    if (!test) {
        log.info("Invalid test name:%s", name);
        return;
    }
    test.run();
}

function buildMenu() {

    let body = document.getElementsByTagName("body");
    for (let name in testMap) {
        console.log(name, testMap[name]);
        let link = document.createElement("a");
        link.href = "?t=" + name;
        link.style.display = "block";
        link.innerHTML = name;
        body[0].appendChild(link);
    }
}

main();

